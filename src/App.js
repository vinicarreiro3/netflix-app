import React, { useEffect, useState } from "react";
import { useContext } from "react";
import { DBContext } from "./Providers/Db";
import Routes from "./Routes";

function App() {
  const [movieList, setMovieList] = useState([]);
  const { Db } = useContext(DBContext);
  //Trocar isso futaramente por axios
  useEffect(() => {
    const loadAll = async () => {
      let list = await Db.Db;
      setMovieList(list);
    };

    loadAll();
  }, []);

  console.log(movieList);
  return (
    <div className="page">
      <Routes />
    </div>
  );
}

export default App;
