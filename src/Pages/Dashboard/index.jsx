import { useState } from "react";
import { useEffect } from "react";
import { useContext } from "react";
import ListCard from "../../components/Lists/Index";
import { DBContext } from "../../Providers/Db";

const Dashboard = () => {
  const [movieList, setMovieList] = useState([]);
  const { Db } = useContext(DBContext);
  //Trocar isso futaramente por axios
  useEffect(() => {
    const loadAll = async () => {
      let list = await Db.Db;
      setMovieList(list);
    };

    loadAll();
  }, []);

  console.log(movieList);
  return (
    <>
      <div>
        <ListCard movieList={movieList} />
      </div>
    </>
  );
};

export default Dashboard;
