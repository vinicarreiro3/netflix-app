import ButtonLink from "../../components/ButtonLink";

const Home = () => {
  return (
    <div className="menu">
      <ButtonLink link="/signup" msg="signup" />
    </div>
  );
};

export default Home;
