import ButtonLink from "../../components/ButtonLink";

const Signin = () => {
  return (
    <>
      <ButtonLink link="/dashboard" msg="dashboard" />
    </>
  );
};

export default Signin;
