import ButtonLink from "../../components/ButtonLink";

const Signup = () => {
  return (
    <>
      <ButtonLink link="/signin" msg="signin" />
    </>
  );
};

export default Signup;
