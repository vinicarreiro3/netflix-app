import { createContext, useState, useEffect } from "react";
import { apiMal } from "../../utils/api";

export const DBContext = createContext();

export const DBProvider = ({ children }) => {
  const [Lists, setLists] = useState([]);

  const addToLists = () => {
    apiMal.get("/anime/1", {}).then((response) => setLists(...Lists, response));
  };

  return <DBContext.Provider value={{ Lists }}>{children}</DBContext.Provider>;
};
