import { DBProvider } from "./Db";

const Providers = ({ children }) => {
  return <DBProvider>{children}</DBProvider>;
};
export default Providers;
