import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";

const ButtonLink = ({ link, msg }) => {
  return (
    <>
      <Link to={link}>
        <Button>{msg}</Button>
      </Link>
    </>
  );
};

export default ButtonLink;
