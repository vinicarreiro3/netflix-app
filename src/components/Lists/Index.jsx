const ListCard = ({ movieList }) => {
  return (
    <>
      {movieList.map((item, key) => (
        <div key={key}>
          <h2 key={key}>{item.title}</h2>
          <div className="Row">
            {console.log(item)}
            {item.items.results.length > 0 &&
              item.items.results.map((item, key) => (
                <img
                  key={key}
                  src={`https://image.tmdb.org/t/p/w300${item.poster_path}`}
                  alt=""
                />
              ))}
          </div>
        </div>
      ))}
      ;
    </>
  );
};

export default ListCard;
